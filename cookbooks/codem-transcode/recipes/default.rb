#
# Cookbook Name:: codem-transcode
# Recipe:: default
#
# Copyright 2015, Mike Bond <mbondfusion@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

include_recipe "git"
include_recipe "nginx"

# static ffmpeg install
ffmpeg_tarball = Chef::Config[:file_cache_path] + "/" + node['ffmpeg']['localfile']

remote_file ffmpeg_tarball do
  source node['ffmpeg']['tarball']
  mode "0644"
end

directory "/tmp/ffmpeg_install" do
  action :create
  recursive true
  only_if { ::File.exists?(ffmpeg_tarball) }
end

# compile and install node
execute "ffmpeg-installer" do
  cwd "/tmp/ffmpeg_install"
  command "tar xf " + ffmpeg_tarball + ";
    find . -type f -executable -print0 | sudo xargs -0 cp -t /usr/bin"
  only_if { ::File.exists?(ffmpeg_tarball) }
end

# clean up from ffmpeg tarball
file ffmpeg_tarball do
  action :delete
  only_if { ::File.exists?("/usr/bin/ffmpeg") }
end

# ensure dependency packages are installed
%w[nscd python-pip].each do |pkg|
  package pkg
end

# pip install eyeD3 package (for v0.7.5-final)
execute "eyeD3-installer" do
  command "sudo pip install eyeD3==0.7.5 -U -q"
end

# setup mysql server
mysql_service 'default' do
  version '5.5'
  bind_address '0.0.0.0'
  port '3306'
  initial_root_password node['mysql']['server_root_password']
  action [:create, :start]
end

# create codem-transcode database
mysql_database 'codem_transcode' do
  connection(
    :host => '127.0.0.1',
    :username => 'root',
    :password => node['mysql']['server_root_password']
  )
  action :create
end

# create the checkout directory
directory node['codem_transcode']['installdir'] do
  owner node['config']['user']
  group node['config']['user']
  recursive true
  action :create
end

# checkout codem-transcode from github
git node['codem_transcode']['installdir'] do
  repository node['codem_transcode']['giturl']
  revision "master"
  action :sync
end

# change ownership of codem-transcode directory
directory node['codem_transcode']['installdir'] do
  owner node['config']['user']
  group node['config']['user']
  recursive true
  action :create
end

# install node from source
nodejs_tarball = Chef::Config[:file_cache_path] + "/" + node['nodejs']['localfile']

remote_file nodejs_tarball do
  source node['nodejs']['tarball']
  mode "0644"
  not_if { ::File.exists?("/usr/local/bin/node") }
end

directory "/tmp/nodejs_install" do
  action :create
  recursive true
  only_if { ::File.exists?(nodejs_tarball) }
end

# compile and install node
execute "nodejs-installer" do
  cwd "/tmp/nodejs_install"
  command "tar xzf " + nodejs_tarball + ";
    cd node-v0.10.36 && ./configure;
    make && sudo make install"
  creates "/usr/local/bin/node"
end

# clean up from node source
file nodejs_tarball do
  action :delete
  only_if { ::File.exists?("/usr/local/bin/node") }
end

# install npm packages to support codem-transcode
execute "codem-packages" do
  cwd node['codem_transcode']['installdir']
#  user node['config']['user']
#  group node['config']['user']
  command "sudo su #{node['config']['user']} -c 'npm install && npm install mysql'"
#  not_if { ::File.directory?("#{node['codem_transcode']['installdir']}/node_modules") }
end

# create the logs directory
directory node['codem_transcode']['logdir'] do
  owner node['config']['user']
  group node['config']['user']
  action :create
  recursive true
end

# create the scratch directory
directory node['codem_transcode']['scratchdir'] do
  owner node['config']['user']
  group node['config']['user']
  action :create
  recursive true
end

# setup codem-transcode config.json
template "#{node['codem_transcode']['installdir']}/config.json" do
  source "codem-transcode-config.erb"
  owner node['config']['user']
  group node['config']['user']
  mode "0644"
  variables(
      :db_password => node['mysql']['server_root_password']
  )
end

# install global pm2 node package
execute "node-pm2" do
  command "sudo npm install -g pm2"
  not_if { ::File.exists?("/usr/local/bin/pm2") }
end

# start pm2 to execute codem-transcode
execute "pm2-codem-transcode" do
  cwd node['codem_transcode']['installdir']
#  user node['config']['user']
#  group node['config']['user']
  command "sudo su #{node['config']['user']} -c 'pm2 start ./bin/codem-transcode --watch -- -c config.json'"
  not_if "sudo su - #{node['config']['user']} -c 'pm2 info codem-transcode'"
end

# start pm2 to execute as boot
execute "pm2-startup" do
  command "sudo pm2 startup ubuntu -u #{node['config']['user']}"
  not_if { ::File.exists?("/etc/init.d/pm2-init.sh") }
end

# update the pm2-init.sh start script to properly reflect codem-transcode user
template "/etc/init.d/pm2-init.sh" do
  source "pm2-init.erb"
  owner "root"
  group "root"
  mode "0755"
  variables(
    :codem_user => node['config']['user']
  )
end

# save pm2 execution
execute "pm2-save" do
  cwd node['codem_transcode']['installdir']
#  user node['config']['user']
#  group node['config']['user']
  command "sudo su #{node['config']['user']} -c 'pm2 save'"
end

# setup nginx to serve codem-transcode application
template "#{node['nginx']['dir']}/sites-available/codem_transcode" do
  source "nginx_codem_site.erb"
  owner "root"
  group "root"
  mode "0644"
  variables(
      :hostname => node[:hostname],
      :fqdn => node[:fqdn]
  )
end

# enable codem_transcode site
nginx_site "codem_transcode" do
  enable true
end

# disable default site
nginx_site "default" do
  enable false
end
