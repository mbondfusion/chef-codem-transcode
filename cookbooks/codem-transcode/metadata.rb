name             'codem-transcode'
maintainer       'Mike Bond'
maintainer_email 'mbondfusion@gmail.com'
license          'All rights reserved'
description      'Installs/Configures codem-transcode'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends "apt"
depends "git"
depends "mysql", "~> 6.0"
depends "database"
depends "nginx"
