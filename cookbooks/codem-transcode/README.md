codem-transcode Cookbook
========================
This cookbook installs the codem-transcode nodejs application.

Requirements
------------
The following Chef cookbooks are required for this cookbook.
Most of the cookbooks listed are only listed because they are
dependencies on other cookbooks.

#### cookbooks
- `apt`
- `bluepill`
- `build-essential`
- `chef-sugar`
- `chef_handler`
- `database`
- `dmg`
- `git`
- `mariadb`
- `mysql`
- `mysql2_chef_gem`
- `nginx`
- `ohai`
- `openssl`
- `postgresql`
- `rbac`
- `rsyslog`
- `runit`
- `smf`
- `windows`
- `yum`
- `yum-epel`
- `yum-mysql-community`


Usage
-----
#### codem-transcode::default

Just include `codem-transcode` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[codem-transcode]"
  ]
}
```

License and Authors
-------------------

Mike Bond <mbondfusion@gmail.com>
